package activityPackage;

public class StrengthTraining extends Activity {
    public StrengthTraining(int time)
    {
        super(time);
        setCaloriesBurnRate(5);
        setHeartIncPercentage(.6);
    }
}
