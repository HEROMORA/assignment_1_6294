package activityPackage;

public class Activity {

    private int time;
    private int caloriesBurnRate;
    private double heartIncPercentage;
    private double heartRate = 80;

    public Activity(int time) {
        this.time = time;
    }

    public void setCaloriesBurnRate(int caloriesBurnRate) {
        this.caloriesBurnRate = caloriesBurnRate;
    }

    public void setHeartIncPercentage(double heartIncPercentage) {
        this.heartIncPercentage = heartIncPercentage;
    }

    public double getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(double heartRate) {
        this.heartRate = heartRate;
    }


    public double calculateIncreaseInBeats(double heartR)
    {
        heartRate = heartR; // debatable if setter method should be called here instead
        double x =  heartR * time * (heartIncPercentage/100);

        // Rounding to the nearest 4 numbers
        x *= 10000;
        x = (int)x;
        x /= 10000;
        return x;
    }

    public int calculateBurntCalories()
    {
        return caloriesBurnRate * time;
    }

    @Override
    public String toString()
    {
        return getClass().getSimpleName() + ", Calories Burnt: " + calculateBurntCalories() +
                ", Heart Rate Inc: " + calculateIncreaseInBeats(heartRate) + ", Current Heart Rate: " + (heartRate);
    }
}
